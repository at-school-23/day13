package ru.devray.day13;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.*;
import static org.openqa.selenium.UnexpectedAlertBehaviour.ACCEPT_AND_NOTIFY;
import static org.openqa.selenium.remote.CapabilityType.UNHANDLED_PROMPT_BEHAVIOUR;

public class SimpleTest {
    //Win+V
    //Ctrl+Shift+V

    //Selenium - средство автоматизации браузера
    //Selenide - средство для автотестирования Web UI

    @Test
    public void test() {

        open("https://demo.yookassa.ru/");
//        Selenide.$()//css locator

        SelenideElement squareProductCard = $x("//img[@alt='Квадрат']");
        squareProductCard.click();

        $x("//button/span[contains(text(), 'В корзину')]").click();

        String squareInCardLineXpath = "//div[@class='cart__area-items']//a[contains(text(), 'Квадрат')]";
        SelenideElement squareWareInShoppingCart = $x(squareInCardLineXpath);
        squareWareInShoppingCart.shouldBe(visible);
        //should, shouldBe, shouldHave

        sleep(3000); //!!!
    }

}
