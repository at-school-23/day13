package ru.devray.day13;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import org.testng.annotations.Test;

import java.time.Duration;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.$x;
import static com.codeborne.selenide.Selenide.open;
import static org.openqa.selenium.UnexpectedAlertBehaviour.ACCEPT_AND_NOTIFY;
import static org.openqa.selenium.remote.CapabilityType.UNHANDLED_PROMPT_BEHAVIOUR;

public class WaitTest {
    @Test(description = "Тест на появление элемента с задержкой (прогрессбар)")
    public void testWaitForElement() {
        Configuration.timeout = 500;

        open("https://the-internet.herokuapp.com/dynamic_loading/1");

        SelenideElement startButton = $x("//button");
        startButton.click();

        SelenideElement finishHeader = $x("//div[@id='finish']/h4");
        finishHeader.should(appear, Duration.ofSeconds(10000));


    }

    @Test(description = "")
    public void testJavascriptAlerts() {
        Configuration.browserCapabilities.setCapability(UNHANDLED_PROMPT_BEHAVIOUR, ACCEPT_AND_NOTIFY);
        open("https://the-internet.herokuapp.com/javascript_alerts");

        SelenideElement jsAlertButton = $x("//button[text()=\"Click for JS Alert\"]");
//        SelenideElement jsAlertButton = $x("//button[text()=\"Click for JS Prompt\"]");
        jsAlertButton.click();

//        $x("//button[text()=\"Click for JS Confirm\"]");
//        $x("//button[text()=\"Click for JS Prompt\"]");

        SelenideElement resultTextBox = $x("//p[@id='result']");
        resultTextBox
                .shouldBe(visible)
                .shouldHave(text("You successfully clicked an alert"));

        Selenide.sleep(200000);
    }
}
